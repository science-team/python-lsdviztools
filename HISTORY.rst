=======
History
=======

0.1.0 (2020-05-01)
------------------

* First release on PyPI.

0.2.0 (2020-06-16)
------------------

* Minor bug fixes

0.3.0 (2020-09-03)
------------------

* Minor bug fixes

0.4.0 (2021-02-03)
------------------

* Updated opentopography API and fixed chi mapping interface

0.4.4 (2021-03-02)
------------------

* Fixed a bug in the projection section of gdalio. Trying to make the code more resistent to errors generate by different versions of proj
