Document: python-lsdviztools
Title: Debian python-lsdviztools Manual
Author: <insert document author here>
Abstract: This manual describes what python-lsdviztools is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/python-lsdviztools/python-lsdviztools.sgml.gz

Format: postscript
Files: /usr/share/doc/python-lsdviztools/python-lsdviztools.ps.gz

Format: text
Files: /usr/share/doc/python-lsdviztools/python-lsdviztools.text.gz

Format: HTML
Index: /usr/share/doc/python-lsdviztools/html/index.html
Files: /usr/share/doc/python-lsdviztools/html/*.html
